/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.Admin;
import Business.Role.AirlinerSupplier;
import Business.Role.Customer;
import Business.Role.Guider;
import Business.Role.HotelSupplier;
import Business.Role.User;
import java.util.ArrayList;

/**
 *
 * @author Brickea
 */
public class Users {

//    private ArrayList<User> usersDirectory;
    private ArrayList<Admin> adminDirectory;
    private ArrayList<AirlinerSupplier> airlinerSupplierDirectory;
    private ArrayList<HotelSupplier> hotelSupplierDirectory;
    private ArrayList<Customer> customerDirectory;
    private ArrayList<Guider> guiderDirectory;

    public Users() {
        this.adminDirectory = new ArrayList<>();
        this.airlinerSupplierDirectory = new ArrayList<>();
        this.customerDirectory = new ArrayList<>();
        this.guiderDirectory = new ArrayList<>();
        this.hotelSupplierDirectory = new ArrayList<>();
    }

    public User getUser(String name, String password, String role) {
        for (User u : this.adminDirectory) {
            if (u.isThisUser(name, password, role) != null) {
                return u;
            }
        }
        for (User u : this.airlinerSupplierDirectory) {
            if (u.isThisUser(name, password, role) != null) {
                return u;
            }
        }
        for (User u : this.customerDirectory) {
            if (u.isThisUser(name, password, role) != null) {
                return u;
            }
        }
        for (User u : this.guiderDirectory) {
            if (u.isThisUser(name, password, role) != null) {
                return u;
            }
        }
        for (User u : this.hotelSupplierDirectory) {
            if (u.isThisUser(name, password, role) != null) {
                return u;
            }
        }
        return null;
    }

    public User addUser(String name, String password, String role) {
        if (this.getUser(name, password, role) != null) {
            return null;
        }
        User u = null;
        if (role.equals("Admin")) {
            u = new Admin(name, password);
            this.adminDirectory.add((Admin) u);
        } else if (role.equals("Customer")) {
            u = new Customer(name, password);
            this.customerDirectory.add((Customer) u);
        } else if (role.equals("Airliner")) {
            u = new AirlinerSupplier(name, password);
            this.airlinerSupplierDirectory.add((AirlinerSupplier) u);
        } else if (role.equals("Hotel")) {
            u = new HotelSupplier(name, password);
            this.hotelSupplierDirectory.add((HotelSupplier) u);
        } else if (role.equals("Guider")) {
            u = new Guider(name, password);
            this.guiderDirectory.add((Guider) u);
        }

        return u;
    }

    public ArrayList<Admin> getAdminDirectory() {
        return adminDirectory;
    }

    public ArrayList<AirlinerSupplier> getAirlinerSupplierDirectory() {
        return airlinerSupplierDirectory;
    }

    public ArrayList<HotelSupplier> getHotelSupplierDirectory() {
        return hotelSupplierDirectory;
    }

    public ArrayList<Customer> getCustomerDirectory() {
        return customerDirectory;
    }

    public ArrayList<Guider> getGuiderDirectory() {
        return guiderDirectory;
    }

}
