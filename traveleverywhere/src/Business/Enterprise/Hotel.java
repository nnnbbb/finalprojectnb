/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Enterprise.product.Room;
import java.util.ArrayList;

/**
 *
 * @author brickeawang
 */
public class Hotel {

    private String companyName;
    private ArrayList<Room> roomProduct;

    public Hotel() {
        this.companyName = "Default name";
        this.roomProduct = new ArrayList<>();
    }

    public Hotel(String name) {
        this.companyName = name;
        this.roomProduct = new ArrayList<>();
    }

    @Override
    public String toString() {
        return this.companyName;
    }

    public Room addRoomTest(String roomName, String type, String date, float price) {
        Room r = new Room(roomName, type, date, price);
        for (Room er : this.roomProduct) {
            if (er.isSameRoom(r)) {
                return null;
            }
        }
        this.roomProduct.add(r);
        return r;
    }

    public Room deleteRoomTest(Room r) {
        for (Room er : this.roomProduct) {
            if (er.isSameRoom(r)) {
                return null;
            }
        }
        this.roomProduct.remove(r);
        return r;
    }

    public ArrayList<Room> getRoomProduct() {
        return roomProduct;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

}
