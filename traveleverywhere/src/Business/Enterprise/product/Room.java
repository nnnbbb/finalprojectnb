/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise.product;

/**
 *
 * @author brickeawang
 */
public class Room {

    private String roomName;
    private String type;
    private String date;
    private float price;

    public Room() {
        this.roomName = "Default room name";
        this.type = "Default type";
        this.date = "Default date";
        this.price = 0;
    }

    public Room(String roomName, String type, String date, float price) {
        this.roomName = roomName;
        this.type = type;
        this.date = date;
        this.price = price;
    }

    @Override
    public String toString() {
        return this.roomName;
    }

    public boolean isSameRoom(Room nr) {
        if (nr.getRoomName().equals(this.roomName) && nr.getPrice() == this.price && nr.getDate().equals(this.date) && nr.getType().equals(this.type)) {
            return true;
        } else {
            return false;
        }
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getType() {
        return type;
    }

    public void setType(String Type) {
        this.type = Type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

}
