/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise.product;

/**
 *
 * @author brickeawang
 */
public class PlanP {

    private String airliner;
    private String hotel;
    private String guider;
    private boolean status;
    private int approvedSeriesNubmer;
    // false pending
    // true approved

    public PlanP() {
        this.status = false;
    }

    public PlanP(String airliner, String hotel, String guider) {
        this.airliner = airliner;
        this.hotel = hotel;
        this.guider = guider;
        this.status = false;
    }

    @Override
    public String toString() {
        return this.airliner;
    }

    public int getApprovedSeriesNubmer() {
        return approvedSeriesNubmer;
    }

    public void setApprovedSeriesNubmer(int approvedSeriesNubmer) {
        this.approvedSeriesNubmer = approvedSeriesNubmer;
    }

    public String getAirline() {
        return airliner;
    }

    public void setAirline(String airliner) {
        this.airliner = airliner;
    }

    public String getHotel() {
        return hotel;
    }

    public void setHotel(String hotel) {
        this.hotel = hotel;
    }

    public String getGuider() {
        return guider;
    }

    public void setGuider(String guider) {
        this.guider = guider;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

}
