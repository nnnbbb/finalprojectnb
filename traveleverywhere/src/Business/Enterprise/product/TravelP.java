/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise.product;

import Business.Role.AirlinerSupplier;
import Business.Role.Guider;
import Business.Role.HotelSupplier;
import java.util.ArrayList;

/**
 *
 * @author brickeawang
 */
public class TravelP {

    int seriesNumber;
    Flight flight;
    Room room;
    AirlinerSupplier airliner;
    HotelSupplier hotel;
    Guider guider;
    float price;
    float profit;

    public TravelP() {
        this.seriesNumber = 0;
        this.flight = new Flight();
        this.room = new Room();
        this.guider = new Guider("default name", "default name");
        this.price = 0;
        this.profit = 0;
    }

    public TravelP(AirlinerSupplier as,HotelSupplier hs,int seriesNumber, Flight flight, Room room, Guider guider, float profit) {
        this.seriesNumber = seriesNumber;
        this.flight = flight;
        this.room = room;
        this.guider = guider;
        this.price = this.flight.getPrice() + this.room.getPrice() + profit;
        this.airliner = as;
        this.hotel = hs;
        this.profit = profit;
    }

    public TravelP(AirlinerSupplier as,HotelSupplier hs,int seriesNumber, Flight flight, Room room, float profit) {
        this.seriesNumber = seriesNumber;
        this.flight = flight;
        this.room = room;
        this.guider = guider;
        this.price = this.flight.getPrice() + this.room.getPrice() + profit;
        this.airliner = as;
        this.hotel = hs;
        this.profit = profit;
    }

    @Override
    public String toString() {
        return String.valueOf(this.seriesNumber);
    }

    public int getSeriesNumber() {
        return seriesNumber;
    }

    public void setSeriesNumber(int seriesNumber) {
        this.seriesNumber = seriesNumber;
    }

    public AirlinerSupplier getAirliner() {
        return airliner;
    }

    public void setAirliner(AirlinerSupplier airliner) {
        this.airliner = airliner;
    }
    
    

    public HotelSupplier getHotel() {
        return hotel;
    }

    public void setHotel(HotelSupplier hotel) {
        this.hotel = hotel;
    }

    public Guider getGuider() {
        return guider;
    }

    public void setGuider(Guider guider) {
        this.guider = guider;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public float getProfit() {
        return profit;
    }

    public void setProfit(float profit) {
        this.profit = profit;
    }
    
    
    

}
