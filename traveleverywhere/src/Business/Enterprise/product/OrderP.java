/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise.product;

import Business.Role.AirlinerSupplier;
import Business.Role.Guider;
import Business.Role.HotelSupplier;

/**
 *
 * @author brickeawang
 */
public class OrderP extends TravelP{
    
    private String date;
    public OrderP(AirlinerSupplier as,HotelSupplier hs,int seriesNumber, Flight flight, Room room, Guider guider, float profit,String date){
        super( as, hs, seriesNumber,  flight,  room,  guider,  profit);
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    
}
