/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise.product;

/**
 *
 * @author brickeawang
 */
public class RoomRequest {
    public String type;
    public String date;
    public boolean state;
    
    public RoomRequest(String type,String date,boolean state){
        this.type = type;
        this.date = date;
        this.state = state;
    }
    
    @Override
    public String toString(){
        return this.type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }
    
}
