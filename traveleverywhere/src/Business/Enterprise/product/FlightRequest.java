/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise.product;

/**
 *
 * @author brickeawang
 */
public class FlightRequest {
    private String route;
    private String date;
    private boolean state;
    
    public FlightRequest(String route,String date,boolean state){
        this.route = route;
        this.date = date;
        this.state = state;
    }
    @Override
    public String toString(){
        return this.route;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean getState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }
    
    
    
}
