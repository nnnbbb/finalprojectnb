/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise.product;

/**
 *
 * @author brickeawang
 */
public class Flight {
    private String flightNumber;
    private String route;
    private String date;
    private float price;

    
    public Flight(){
        this.flightNumber = "Default flight nubmer";
        this.route = "Default route";
        this.date = "Default date";
        this.price = 0;
    }
    public Flight(String flightNumber,String route,String date,float price){
        this.flightNumber = flightNumber;
        this.route = route;
        this.date = date;
        this.price = price;
    }
    @Override
    public String toString(){
        return this.flightNumber;
    }
    
    public boolean isSameFlight(Flight nf) {
        if (nf.getFlightNumber().equals(this.flightNumber) && nf.getPrice() == this.price && nf.getDate().equals(this.date) && nf.getRoute().equals(this.route)) {
            return true;
        } else {
            return false;
        }
    }
    
    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
