/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Enterprise.product.Flight;
import java.util.ArrayList;

/**
 *
 * @author brickeawang
 */
public class Airliner {

    private String companyName;
    private ArrayList<Flight> flightProduct;

    public Airliner() {
        this.companyName = "Default name";
        this.flightProduct = new ArrayList<>();
    }

    public Airliner(String name) {
        this.companyName = name;
        this.flightProduct = new ArrayList<>();
    }

    @Override
    public String toString() {
        return this.companyName;
    }

    public Flight addFlightTest(String flightNuber, String route, String date, float price) {
        Flight f = new Flight(flightNuber, route, date, price);
        for (Flight ef : this.flightProduct) {
            if (ef.isSameFlight(f)) {
                return null;
            }
        }
        this.flightProduct.add(f);
        return f;
    }

    public Flight deleteFlightTest(Flight f) {
        for (Flight ef : this.flightProduct) {
            if (ef.isSameFlight(f)) {
                return null;
            }
        }
        this.flightProduct.remove(f);
        return f;
    }

    public ArrayList<Flight> getFlightProduct() {
        return flightProduct;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

}
