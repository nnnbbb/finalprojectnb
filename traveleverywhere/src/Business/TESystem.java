/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.DB4OUtil.DB4OUtil;
import Business.Enterprise.product.Flight;
import Business.Enterprise.product.OrderP;
import Business.Enterprise.product.PlanP;
import Business.Enterprise.product.Room;
import Business.Enterprise.product.TravelP;
import Business.Organization.Users;
import Business.Role.Admin;
import Business.Role.AirlinerSupplier;
import Business.Role.Customer;
import Business.Role.HotelSupplier;
import Business.Role.TEAdmin;
import Business.Role.User;

/**
 *
 * @author riverlin
 */
public class TESystem {

    private static TESystem business;
    private Users users;

    public Users getUsers() {
        // The role will contain 
        // "Admin"
        // "Customer"
        // "Guider"
        // getUsers().getUser(name,password,role) will return a User object if it is exist. If it isn't exist will return null
        return this.users;
    }
    
    public static void showAllCustomerData(){
        System.out.println("------------------Customer Data-------------------");
        for(Customer c :TESystem.getDatabase().getUsers().getCustomerDirectory()){
            System.out.println(c.getName());
            System.out.println("Ser \t Airliner \t Hotel \t Flight \t Room \t Price \t Date");
            for(OrderP p:c.getOrderP()){
                System.out.println(p.getSeriesNumber()+"\t"+p.getAirliner()+"\t"+p.getHotel()+"\t"+p.getFlight()+"\t"+p.getRoom()+"\t"+p.getPrice()+"\t"+p.getDate());
            }
            System.out.println("Airliner \t Hotel \t  Guider \t State");
            for(PlanP p:c.getPlanP()){
                System.out.println(p.getAirline()+"\t"+p.getHotel()+"\t"+p.getGuider()+"\t"+p.getStatus());
                
            }
        }
        System.out.println("------------------Customer Data-------------------");
    }
    public static void showAllTravelProduct(){
        System.out.println("------------------Travel Product-------------------");
        System.out.println("Series\t Airliner\t Hotel\t Guider\t Price\t Flight\t Room");
        for(TravelP p:TESystem.getDatabase().getTEAdmin().getProductDirectory()){
            System.out.println(p.getSeriesNumber()+"\t"+p.getAirliner()+"\t"+p.getHotel()+"\t"+p.getGuider()+"\t"+p.getPrice()+"\t"+p.getFlight()+"\t"+p.getRoom());
        }
        
    }

    public static void showAllUserData() {
        System.out.println("------------------Users Detial------------------");
        System.out.println("Name\t\tPasswordt\t\tRole");
        for (User u : TESystem.getDatabase().getUsers().getAdminDirectory()) {
            System.out.println(u.getName() + "\t\t" + u.getPassword() + "\t\t" + u.getRole());
        }
        for (User u : TESystem.getDatabase().getUsers().getAirlinerSupplierDirectory()) {
            System.out.println(u.getName() + "\t\t" + u.getPassword() + "\t\t" + u.getRole());
        }
        for (User u : TESystem.getDatabase().getUsers().getHotelSupplierDirectory()) {
            System.out.println(u.getName() + "\t\t" + u.getPassword() + "\t\t" + u.getRole());
        }
        for (User u : TESystem.getDatabase().getUsers().getCustomerDirectory()) {
            System.out.println(u.getName() + "\t\t" + u.getPassword() + "\t\t" + u.getRole());
        }
        for (User u : TESystem.getDatabase().getUsers().getGuiderDirectory()) {
            System.out.println(u.getName() + "\t\t" + u.getPassword() + "\t\t" + u.getRole());
        }
        
        System.out.println("------------------Users Detial------------------");
    }

    public static void showAllSupplierData() {
        System.out.println("------------------Supplier Detial----------------------");
        for (AirlinerSupplier as : TESystem.getDatabase().getUsers().getAirlinerSupplierDirectory()) {
            System.out.println(as.getName() + "\t\t" + as.getPassword() + "\t\t" + as.getRole());
            for (Flight f : as.getCompany().getFlightProduct()) {
                System.out.println(f.getFlightNumber() + "\t\t" + f.getDate() + "\t\t" + f.getRoute() + "\t\t" + f.getPrice());
            }
        }

        for (HotelSupplier hs : TESystem.getDatabase().getUsers().getHotelSupplierDirectory()) {
            System.out.println(hs.getName() + "\t\t" + hs.getPassword() + "\t\t" + hs.getRole());
            for (Room r : hs.getCompany().getRoomProduct()) {
                System.out.println(r.getRoomName()+ "\t\t" + r.getDate() + "\t\t" + r.getType()+ "\t\t" + r.getPrice());
            }
        }
        System.out.println("------------------Supplier Detial----------------------");
    }

    public static TESystem getDatabase() {
        if (business == null) {
            business = new TESystem();
        }
        System.out.println(business.getUsers());
        return business;
    }

    private TESystem() {
        this.users = new Users();
    }

    public static void readData() {
        System.out.println("Connect to the database! Read all data. ");
        business = DB4OUtil.getInstance().retrieveSystem();
        System.out.println("-----------Success!-----------");

    }

    public static void saveData() {
        System.out.println("Connect to the database! Save all data. ");
        DB4OUtil.getInstance().storeSystem(business);
        System.out.println("-----------Success!-----------");

    }
    public Admin getTEAdmin(){
        for(Admin a :this.getUsers().getAdminDirectory()){
            if(a.getName().equals("teadmin1")){
                return a;
            }
        }
        return null;
    }

}
