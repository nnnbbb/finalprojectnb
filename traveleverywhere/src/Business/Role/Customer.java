/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.Enterprise.product.OrderP;
import Business.Enterprise.product.PlanP;
import java.util.ArrayList;

/**
 *
 * @author brickeawang
 */
public class Customer extends User{
    private String gender;
    private String age;
    private ArrayList<OrderP> orderP;
    private ArrayList<PlanP> planP;
    public Customer(String name,String password){
        super(name,password,"Customer");
        this.gender = "not set";
        this.age = "not set";
        this.orderP = new ArrayList<>();
        this.planP = new ArrayList<>();
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public ArrayList<OrderP> getOrderP() {
        return orderP;
    }

    public void setOrderP(ArrayList<OrderP> orderP) {
        this.orderP = orderP;
    }

    public ArrayList<PlanP> getPlanP() {
        return planP;
    }

    public void setPlanP(ArrayList<PlanP> planP) {
        this.planP = planP;
    }
    
    
    
    
}
