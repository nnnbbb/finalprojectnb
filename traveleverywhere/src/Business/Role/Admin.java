/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.Enterprise.product.Flight;
import Business.Enterprise.product.Room;
import Business.Enterprise.product.TravelP;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.Iterator;

/**
 *
 * @author Brickea
 */
public class Admin extends User {

    private ArrayList<TravelP> productDirectory;
    private ArrayList<Integer> productSeries;
    

    public Admin(String name, String password) {
        super(name, password, "Admin");
        this.productDirectory = new ArrayList<>();
        this.productSeries = new ArrayList<>();
    }

    public TravelP addProductWithGuider(AirlinerSupplier as, HotelSupplier hs,Flight flight, Room room, Guider guider, float profit) {
        Integer i = 1;
        boolean exist = false;
        for (i = 1; i < 1000; i++) {
            for (Integer j : this.productSeries) {
                if (j.toString().equals(i.toString())) {
                    exist = true;
                    break;
                }
            }
            if (!exist) {
                break;
            }
            exist = false;
        }
        TravelP tp = new TravelP(as,hs,i, flight, room, guider, profit);
        guider.getProject().add(tp);
        this.productSeries.add(i);
        this.productDirectory.add(tp);
        return tp;
    }

    public TravelP addProductWithoutGuider(AirlinerSupplier as, HotelSupplier hs,Flight flight, Room room, float profit) {
        Integer i = 1;
        boolean exist = false;
        for (i = 1; i < 1000; i++) {
            for (Integer j : this.productSeries) {
                if (j.toString().equals(i.toString())) {
                    exist = true;
                    break;
                }
            }
            if (!exist) {
                break;
            }
            exist = false;
        }
        TravelP tp = new TravelP(as,hs,i, flight, room, profit);
        this.productSeries.add(i);
        this.productDirectory.add(tp);
        return tp;
    }

    public TravelP deleteProduct(TravelP p) {
        Iterator<Integer> iterator = this.productSeries.iterator();
        while(iterator.hasNext()){
            Integer integer = iterator.next();
            if(integer==p.getSeriesNumber())
                iterator.remove();   //注意这个地方
        }
        this.productDirectory.remove(p);
        return p;

    }

    public ArrayList<TravelP> getProductDirectory() {
        return productDirectory;
    }

}
