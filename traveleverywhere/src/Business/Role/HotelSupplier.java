/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.Enterprise.Hotel;
import Business.Enterprise.product.RoomRequest;
import java.util.ArrayList;

/**
 *
 * @author brickeawang
 */
public class HotelSupplier extends User{
    private Hotel company;
    private ArrayList<RoomRequest> roomRequestDirectory;
    
    public HotelSupplier(String name,String password){
        super(name,password,"Hotel");
        this.company = new Hotel();
        this.roomRequestDirectory = new ArrayList<>();
    }

    public Hotel getCompany() {
        return company;
    }

    public ArrayList<RoomRequest> getRoomRequestDirectory() {
        return roomRequestDirectory;
    }
    
    
    
}
