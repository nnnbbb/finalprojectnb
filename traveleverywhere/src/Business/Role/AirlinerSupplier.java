/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.Enterprise.Airliner;
import Business.Enterprise.product.FlightRequest;
import java.util.ArrayList;

/**
 *
 * @author brickeawang
 */
public class AirlinerSupplier extends User{
    
    private Airliner company;
    private ArrayList<FlightRequest> flightRequestDirectory;
    
    public AirlinerSupplier(String name,String password){
        super(name,password,"Airliner");
        this.company = new Airliner();
        this.flightRequestDirectory = new ArrayList<>();
    }

    public Airliner getCompany() {
        return company;
    }

    public ArrayList<FlightRequest> getFlightRequestDirectory() {
        return flightRequestDirectory;
    }
    
    
    
    
}
