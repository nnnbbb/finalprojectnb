/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.Enterprise.product.TravelP;
import java.util.ArrayList;

/**
 *
 * @author brickeawang
 */
public class Guider extends User{
    
    String gender;
    ArrayList<TravelP> project;
    public Guider(String name,String password){
        super(name,password,"Guider");
        this.gender = "not set";
        this.project = new ArrayList<>();
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public ArrayList<TravelP> getProject() {
        return project;
    }

    public void setProject(ArrayList<TravelP> project) {
        this.project = project;
    }

    
    
}
