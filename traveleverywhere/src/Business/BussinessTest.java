/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Enterprise.Airliner;
import Business.Enterprise.product.Flight;
import Business.Enterprise.product.OrderP;
import Business.Enterprise.product.PlanP;
import Business.Enterprise.product.Room;
import Business.Role.Admin;
import Business.Role.AirlinerSupplier;
import Business.Role.Customer;
import Business.Role.Guider;
import Business.Role.HotelSupplier;
import Business.Role.User;

/**
 *
 * @author Brickea
 */
public class BussinessTest {
//
//    public static void main(String args[]) {
//        System.out.println("BussinessTest start");
//        TESystem.readData();
//        TESystem.showAllUserData();
//        
//        System.out.println("Customed test start------------------");
//        BussinessTest.addTestData();
//        TESystem.showAllSupplierData();
//        TESystem.saveData();
////        System.out.println("BussinessTest read data start");
////        TES.readData();
////        BussinessTest.testSaveData();
////        TES.saveData();
//
//    }

    public static void dataBaseTest() {
        System.out.println("BussinessTest start");
        TESystem.readData();
        TESystem.showAllUserData();

        System.out.println("Customed test start------------------");
        BussinessTest.addTestData();
//        BussinessTest.addCustomerOPP();
//        TESystem.showAllSupplierData();
//        TESystem.showAllTravelProduct();
//        TESystem.showAllCustomerData();

        TESystem.saveData();
//        System.out.println("BussinessTest read data start");
//        TES.readData();
//        BussinessTest.testSaveData();
//        TES.saveData();
    }

    public static void addTestData() {
        System.out.println("add some test user data---------------");
        BussinessTest.addTEAdmin(1);
//        for (int i = 1; i <= 4; i++) {
//            BussinessTest.addCustomer(i);
//            BussinessTest.addAirlinerSupplier(i);
//            BussinessTest.addHotelSupplier(i);
//            BussinessTest.addGudier(i);
//        }

        System.out.println("add some test user data---------------");
//        BussinessTest.addProduct();
    }

    public static void addCustomerOPP() {
        for (Customer c : TESystem.getDatabase().getUsers().getCustomerDirectory()) {
            for (int i = 4; i <= 6; i++) {
                String airliner = "as" + i + "company";
                String hotel = "hs" + i + "company";
                String guider = "g" + i;
                PlanP p = new PlanP(airliner, hotel, guider);
                c.getPlanP().add(p);

                AirlinerSupplier as = TESystem.getDatabase().getTEAdmin().getProductDirectory().get(i - 3).getAirliner();
                HotelSupplier hs = TESystem.getDatabase().getTEAdmin().getProductDirectory().get(i - 3).getHotel();
                int seriesNumber = TESystem.getDatabase().getTEAdmin().getProductDirectory().get(i - 3).getSeriesNumber();
                Flight flight = TESystem.getDatabase().getTEAdmin().getProductDirectory().get(i - 3).getFlight();
                Room room = TESystem.getDatabase().getTEAdmin().getProductDirectory().get(i - 3).getRoom();
                Guider guideri = TESystem.getDatabase().getTEAdmin().getProductDirectory().get(i - 3).getGuider();
                float profit = TESystem.getDatabase().getTEAdmin().getProductDirectory().get(i - 3).getPrice();
                OrderP o = new OrderP(as, hs, seriesNumber, flight, room, guideri, profit, "2019-11-11");
                c.getOrderP().add(o);

            }

        }
    }

    public static void addProduct() {
        System.out.println("Add some products-----------------------");
        for (int i = 1; i <= 3; i++) {
            String name = "as" + i;
            Flight testF = new Flight("testflight" + i, "testrote" + i, "testdate" + i, 0);
            AirlinerSupplier as = null;
            for (AirlinerSupplier a : TESystem.getDatabase().getUsers().getAirlinerSupplierDirectory()) {
                if (a.getName().equals(name)) {
                    as = a;
                    as.getCompany().setCompanyName(name + "company");
                }
            }

            BussinessTest.airlinerSupplierAddFlight(name, testF);

            name = "hs" + i;
            Room testR = new Room("testroom" + i, "testtype" + i, "testdate" + i, 0);
            HotelSupplier hs = null;
            for (HotelSupplier h : TESystem.getDatabase().getUsers().getHotelSupplierDirectory()) {
                if (h.getName().equals(name)) {
                    hs = h;
                    hs.getCompany().setCompanyName(name + "company");
                }
            }
            BussinessTest.hotelSupplierAddRoom(name, testR);
            for (Guider g : TESystem.getDatabase().getUsers().getGuiderDirectory()) {
                if (g.getName().equals("g1")) {
                    TESystem.getDatabase().getTEAdmin().addProductWithGuider(as, hs, testF, testR, g, 0);
                    TESystem.getDatabase().getTEAdmin().addProductWithGuider(as, hs, testF, testR, g, 1);
                    TESystem.getDatabase().getTEAdmin().addProductWithGuider(as, hs, testF, testR, g, 2);
                    TESystem.getDatabase().getTEAdmin().addProductWithGuider(as, hs, testF, testR, g, 3);
                }
            }
        }

        System.out.println("Add some products-----------------------");
    }

    public static void hotelSupplierAddRoom(String name, Room r) {
        System.out.println("Room add test----------------------------");
        HotelSupplier result = null;
        for (HotelSupplier chs : TESystem.getDatabase().getUsers().getHotelSupplierDirectory()) {
            if (chs.getName().equals(name)) {
                if (chs.getCompany().addRoomTest(r.getRoomName(), r.getType(), r.getDate(), r.getPrice()) != null) {
                    System.out.println("room add success!");
                    result = chs;
                } else {
                    System.out.println("room add failure！");
                }
            }
        }

        System.out.println("Room add test----------------------------");
    }

    public static void airlinerSupplierAddFlight(String name, Flight f) {
        System.out.println("Flight add test----------------------------");
        AirlinerSupplier result = null;
        for (AirlinerSupplier aas : TESystem.getDatabase().getUsers().getAirlinerSupplierDirectory()) {
            if (aas.getName().equals(name)) {
                if (aas.getCompany().addFlightTest(f.getFlightNumber(), f.getRoute(), f.getDate(), f.getPrice()) != null) {
                    System.out.println("room add success!");
                    result = aas;
                } else {
                    System.out.println("room add failure！");
                }
            }
        }
        System.out.println("Flight add test----------------------------");

    }

    public static void addTEAdmin(int number) {
        String name = "teadmin" + number;
        String password = "teadmin" + number;
        String role = "Admin";
        if (TESystem.getDatabase().getUsers().getUser(name, password, role) == null) {
            System.out.println(TESystem.getDatabase().getUsers().addUser(name, password, role));
        } else {
            System.out.println("The TEAdmin is existd!");
        }

    }

    public static void addHotelSupplier(int number) {
        String name = "hs" + number;
        String password = "hs" + number;
        String role = "Hotel";
        if (TESystem.getDatabase().getUsers().getUser(name, password, role) == null) {
            System.out.println(TESystem.getDatabase().getUsers().addUser(name, password, role));
        } else {
            System.out.println("The hotelSupplier is existd!");
        }
    }

    public static void addAirlinerSupplier(int number) {
        String name = "as" + number;
        String password = "as" + number;
        String role = "Airliner";
        if (TESystem.getDatabase().getUsers().getUser(name, password, role) == null) {
            System.out.println(TESystem.getDatabase().getUsers().addUser(name, password, role));
        } else {
            System.out.println("The airlinerSupplier is existd!");
        }
    }

    public static void addCustomer(int number) {
        String name = "c" + number;
        String password = "c" + number;
        String role = "Customer";
        if (TESystem.getDatabase().getUsers().getUser(name, password, role) == null) {
            System.out.println(TESystem.getDatabase().getUsers().addUser(name, password, role));

        } else {
            System.out.println("The customer is existd!");
        }
    }

    public static void addGudier(int number) {
        String name = "g" + number;
        String password = "g" + number;
        String role = "Guider";
        if (TESystem.getDatabase().getUsers().getUser(name, password, role) == null) {
            System.out.println(TESystem.getDatabase().getUsers().addUser(name, password, role));
        } else {
            System.out.println("The guider is existd!");
        }
    }

    public static void testSaveData() {
        System.out.println("test save data--------------------------");
        String name = "teadmin1";
        String password = "teadmin1";
        String role = "Admin";
        System.out.println("test save data: " + TESystem.getDatabase().getUsers().getUser(name, password, role).getName());
        System.out.println("test save data--------------------------");
    }

}
