/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.accountmanage;

import Business.Enterprise.Airliner;
import Business.Role.AirlinerSupplier;
import Business.Role.HotelSupplier;
import Business.Role.User;
import Business.TESystem;
import java.awt.CardLayout;
import java.util.HashSet;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author riverlin
 */
public class stJPanel extends javax.swing.JPanel {

    /**
     * Creates new form stJPanel
     */
    private JPanel panelRight;

    public stJPanel(JPanel panelRight) {
        initComponents();
        this.panelRight = panelRight;
        this.populateInitTable();

//To change body of generated methods, choose Tools | Templates.
    }

    public void populateInitTable() {

        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
        dtm.setRowCount(0);

        for (AirlinerSupplier airliner : TESystem.getDatabase().getUsers().getAirlinerSupplierDirectory()) {

            Object[] row = new Object[2];
            System.out.println(airliner.getName());
            row[0] = airliner;
            row[1] = airliner.getRole();

            dtm.addRow(row);

        }
        for (HotelSupplier Hotel : TESystem.getDatabase().getUsers().getHotelSupplierDirectory()) {

            Object[] row = new Object[2];
            System.out.println(Hotel.getName());
            row[0] = Hotel;
            row[1] = Hotel.getRole();

            dtm.addRow(row);

        }
    }
//    private void populateTable(){
//        //cbbrand.setModel(new javax.swing.DefaultComboBoxModel<>(new String[]{"all"}));
//       
//        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
//        dtm.setRowCount(0);
//       
//        
//        
//        
//        for(Ticket ticket : ticketdirectory.getTicketlist()){
//            Object [] row = new Object [3];
//            String i = null;
//            
//            row[0] = ticket.getFlight();
//            row[1] = ticket.getX();
//            if(ticket.getY()==0){
//                i= "A";
//            }if(ticket.getY()==1){
//                i= "B";
//            }if(ticket.getY()==2){
//                i= "C";
//            }if(ticket.getY()==3){
//                i= "D";
//            }if(ticket.getY()==4){
//                i= "E";
//            }if(ticket.getY()==5){
//                i= "F";
//            }
//            row[2] = i;
//            dtm.addRow(row);
//        
//        }
//    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        detailbtn = new javax.swing.JButton();
        createbtn = new javax.swing.JButton();
        deletebtn = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jButton7 = new javax.swing.JButton();

        setBackground(new java.awt.Color(204, 217, 216));
        setPreferredSize(new java.awt.Dimension(900, 587));

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Account Name", "Company Type"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(1).setResizable(false);
        }

        detailbtn.setBackground(new java.awt.Color(255, 255, 255));
        detailbtn.setText("Detail");
        detailbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                detailbtnActionPerformed(evt);
            }
        });

        createbtn.setBackground(new java.awt.Color(237, 219, 154));
        createbtn.setText("Create");
        createbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createbtnActionPerformed(evt);
            }
        });

        deletebtn.setBackground(new java.awt.Color(240, 167, 167));
        deletebtn.setText("Delete");
        deletebtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deletebtnActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Lucida Grande", 0, 24)); // NOI18N
        jLabel1.setText("Supplier");

        jButton7.setText("< Back");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addComponent(jButton7)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 720, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(deletebtn, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(detailbtn, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(createbtn, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(403, 403, 403)
                        .addComponent(jLabel1)))
                .addContainerGap(49, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addComponent(jLabel1)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 317, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addGap(18, 18, 18)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(detailbtn, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(deletebtn, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(90, 90, 90))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(createbtn, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        // TODO add your handling code here:
        CardLayout layout = (CardLayout) this.panelRight.getLayout();
        this.panelRight.remove(this);
        layout.previous(this.panelRight);
    }//GEN-LAST:event_jButton7ActionPerformed

    private void detailbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_detailbtnActionPerformed
        // TODO add your handling code here:
        int selectRow = this.jTable1.getSelectedRow();
        if (selectRow >= 0) {
            if (this.jTable1.getValueAt(selectRow, 1).equals("Airliner")) {
                stcJPanela mm = new stcJPanela(this.panelRight, (AirlinerSupplier) this.jTable1.getValueAt(selectRow, 0));
                CardLayout layout = (CardLayout) panelRight.getLayout();
                panelRight.add(mm);
                layout.next(panelRight);
            } else if (this.jTable1.getValueAt(selectRow, 1).equals("Hotel")) {
                stchJPanela mm = new stchJPanela(this.panelRight, (HotelSupplier) this.jTable1.getValueAt(selectRow, 0));
                CardLayout layout = (CardLayout) panelRight.getLayout();
                panelRight.add(mm);
                layout.next(panelRight);
            }
        }else {
            JOptionPane.showMessageDialog(null, "Please select a row!");
        }

    }//GEN-LAST:event_detailbtnActionPerformed

    private void createbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createbtnActionPerformed
        // TODO add your handling code here:
        stdJPanel mm = new stdJPanel(this.panelRight, this);
        CardLayout layout = (CardLayout) panelRight.getLayout();
        panelRight.add(mm);
        layout.next(panelRight);
    }//GEN-LAST:event_createbtnActionPerformed

    private void deletebtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deletebtnActionPerformed
        // TODO add your handling code here:

        int selectRow = this.jTable1.getSelectedRow();
        if (selectRow >= 0) {
            int dialogButton = JOptionPane.YES_NO_CANCEL_OPTION;
            int dialogResult = JOptionPane.showConfirmDialog(null, "Are you sure?", "Delete this record", dialogButton);
            if (dialogResult == JOptionPane.YES_OPTION) {
                User d = (User) this.jTable1.getValueAt(selectRow, 0);
                TESystem.getDatabase().getUsers().getAirlinerSupplierDirectory().remove(d);
                TESystem.getDatabase().getUsers().getHotelSupplierDirectory().remove(d);
                populateInitTable();
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please select a row!");
        }
    }//GEN-LAST:event_deletebtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton createbtn;
    private javax.swing.JButton deletebtn;
    private javax.swing.JButton detailbtn;
    private javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
