/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.CustomerRole;

import Business.Enterprise.product.Flight;
import Business.Enterprise.product.Room;
import Business.Enterprise.product.OrderP;
import Business.Role.AirlinerSupplier;
import Business.Role.Customer;
import Business.Role.Guider;
import Business.Role.HotelSupplier;
import Business.Role.User;
import Business.TESystem;
import java.awt.CardLayout;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author riverlin
 */
public class moreOrderJPanel extends javax.swing.JPanel {

    /**
     * Creates new form ctJPanel
     */
    private JPanel panelRight;
    private OrderP op;
    private User u;
    private Customer uc;

    moreOrderJPanel(JPanel panelRight, User u, OrderP op) {
        initComponents();
        this.panelRight = panelRight;
        jLabel1.setText(" Dear " + u.getName().toString() + ", please change your order :");
        this.u = u;
        this.op = op;
        this.populateInitTable();
        for (Customer c : TESystem.getDatabase().getUsers().getCustomerDirectory()) {
            if (c.getName().equals(u.getName())) {
                this.uc = c;
            }
        }
    }
   

    public void populateInitTable() {

        for (AirlinerSupplier a : TESystem.getDatabase().getUsers().getAirlinerSupplierDirectory()) {
            if (op.getAirliner().equals(a)) {
                for (Flight f : a.getCompany().getFlightProduct()) {
                    jComboBox1.addItem(f.getFlightNumber());
                }
            }

        }
        for (HotelSupplier hs : TESystem.getDatabase().getUsers().getHotelSupplierDirectory()) {
            if (op.getHotel().equals(hs)) {
                for (Room r : hs.getCompany().getRoomProduct()) {
                    jComboBox2.addItem(r.getRoomName());
                }
            }

        }

//        for (AirlinerSupplier a : TESystem.getDatabase().getUsers().getAirlinerSupplierDirectory()) {
//            if (op.getAirliner().equals(a)) {
//                jComboBox1.setSelectedItem(a.getCompany().getFlightProduct().get(0));
//
//            }
//
//        }
//        for (HotelSupplier hs : TESystem.getDatabase().getUsers().getHotelSupplierDirectory()) {
//            if (op.getHotel().equals(hs)) {
//                for (Room r : hs.getCompany().getRoomProduct()) {
//                    jComboBox2.setSelectedItem(hs.getCompany().getRoomProduct().get(0));
//                }
//
//            }
//        }
        SimpleDateFormat sdf = new SimpleDateFormat();// Format time
        sdf.applyPattern("yyyy-MM");
        Date date = new Date();// get current time
        String yearMonth = sdf.format(date);
        sdf.applyPattern("dd");
        String day = sdf.format(date);
        int dayi = Integer.parseInt(day);
        for (int i = 0; i < 10; i++) {
            if (dayi < 10) {
                day = "0" + String.valueOf(dayi);
            } else {
                day = String.valueOf(dayi);
            }
            this.jComboBox3.addItem(yearMonth + "-" + day);
            dayi++;
        }                                                                                                                               
        this.jTextField1.setText(String.valueOf(this.op.getPrice()));
        this.jTextField1.setEnabled(false);

//        this.jTextField1.setText(((Flight)this.jComboBox1.getSelectedItem()).getPrice()+((Room)this.jComboBox2.getSelectedItem()).getPrice()+"$");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        placeBtn = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jComboBox2 = new javax.swing.JComboBox<>();
        jComboBox3 = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();

        setBackground(new java.awt.Color(204, 217, 216));
        setPreferredSize(new java.awt.Dimension(900, 600));

        jLabel1.setFont(new java.awt.Font("Lucida Grande", 0, 24)); // NOI18N
        jLabel1.setText("Welcome! Dear  ");

        placeBtn.setBackground(new java.awt.Color(235, 181, 146));
        placeBtn.setText("Place Order");
        placeBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                placeBtnActionPerformed(evt);
            }
        });

        jButton7.setBackground(new java.awt.Color(255, 255, 255));
        jButton7.setText("Back");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        jLabel2.setText("Flight :");

        jComboBox1.setBackground(new java.awt.Color(255, 255, 255));
        jComboBox1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBox1ItemStateChanged(evt);
            }
        });
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        jLabel3.setText("Room:");

        jLabel4.setText("Date:");

        jComboBox2.setBackground(new java.awt.Color(255, 255, 255));
        jComboBox2.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBox2ItemStateChanged(evt);
            }
        });

        jComboBox3.setBackground(new java.awt.Color(255, 255, 255));
        jComboBox3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox3ActionPerformed(evt);
            }
        });

        jLabel5.setText("Price: ");

        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(placeBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(165, 165, 165))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(287, 287, 287)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5))
                        .addGap(132, 132, 132)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jComboBox1, 0, 109, Short.MAX_VALUE)
                            .addComponent(jComboBox2, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jTextField1)
                            .addComponent(jComboBox3, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(218, 218, 218)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addComponent(jButton7)))
                .addContainerGap(328, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(55, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(12, 12, 12)
                .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(51, 51, 51)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(55, 55, 55)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(40, 40, 40)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(67, 67, 67)
                .addComponent(placeBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(70, 70, 70))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        // TODO add your handling code here:
        CardLayout layout = (CardLayout) this.panelRight.getLayout();
        this.panelRight.remove(this);
        layout.previous(this.panelRight);
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        // TODO add your handling code here:

    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void placeBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_placeBtnActionPerformed
        // TODO add your handling code here:
        Flight flight = null;
        for (Flight f : this.op.getAirliner().getCompany().getFlightProduct()) {
            if (f.getFlightNumber().equals((String) this.jComboBox1.getSelectedItem())) {
                flight = f;
            }
        }
        Room room = null;
        for (Room r : this.op.getHotel().getCompany().getRoomProduct()) {
            if (r.getRoomName().equals((String) this.jComboBox2.getSelectedItem())) {
                room = r;
            }
        }
        String date = (String) this.jComboBox3.getSelectedItem();
        float price = flight.getPrice() + room.getPrice() + this.op.getProfit();

        
        this.op.setFlight(flight);
        this.op.setRoom(room);
        this.op.setDate(date);
        this.op.setPrice(price);
        JOptionPane.showMessageDialog(null, "Order has been changed!!");
        this.jButton7ActionPerformed(evt);

    }//GEN-LAST:event_placeBtnActionPerformed

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_jTextField1ActionPerformed

    private void jComboBox1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox1ItemStateChanged
        // TODO add your handling code here:
        Flight flight = null;
        if(flight ==null){
            flight = this.op.getAirliner().getCompany().getFlightProduct().get(0);
        }
        for (Flight f : this.op.getAirliner().getCompany().getFlightProduct()) {
            if (f.getFlightNumber().equals((String) this.jComboBox1.getSelectedItem())) {
                flight = f;
            }
        }
        Room room = null;
        if(room ==null){
            room  = this.op.getHotel().getCompany().getRoomProduct().get(0);
        }
        for (Room r : this.op.getHotel().getCompany().getRoomProduct()) {
            if (r.getRoomName().equals((String) this.jComboBox2.getSelectedItem())) {
                room = r;
            }
        }
        float price = flight.getPrice() + room.getPrice() + this.op.getProfit();
        this.jTextField1.setText(String.valueOf(price));

    }//GEN-LAST:event_jComboBox1ItemStateChanged

    private void jComboBox2ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox2ItemStateChanged
        // TODO add your handling code here:
        Flight flight = null;
        for (Flight f : this.op.getAirliner().getCompany().getFlightProduct()) {
            if (f.getFlightNumber().equals((String) this.jComboBox1.getSelectedItem())) {
                flight = f;
            }
        }
        Room room = null;
        for (Room r : this.op.getHotel().getCompany().getRoomProduct()) {
            if (r.getRoomName().equals((String) this.jComboBox2.getSelectedItem())) {
                room = r;
            }
        }
        float price = flight.getPrice() + room.getPrice() + this.op.getProfit();
        this.jTextField1.setText(String.valueOf(price));
    }//GEN-LAST:event_jComboBox2ItemStateChanged

    private void jComboBox3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBox3ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton7;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JComboBox<String> jComboBox2;
    private javax.swing.JComboBox<String> jComboBox3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JButton placeBtn;
    // End of variables declaration//GEN-END:variables
}
