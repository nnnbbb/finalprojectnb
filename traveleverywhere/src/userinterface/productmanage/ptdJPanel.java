/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.productmanage;

import Business.Enterprise.product.Flight;
import Business.Enterprise.product.Room;
import Business.Enterprise.product.TravelP;
import Business.Role.AirlinerSupplier;
import Business.Role.Guider;
import Business.Role.HotelSupplier;
import Business.TESystem;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author riverlin
 */
public class ptdJPanel extends javax.swing.JPanel {

    /**
     * Creates new form ptdJPanel
     */
    private JPanel panelRight;
    private ptJPanel before;
    private TravelP p;

    public ptdJPanel(JPanel panelRight) {
        initComponents();
        this.panelRight = panelRight;
//        this.initContext();

//To change body of generated methods, choose Tools | Templates.
    }

    public ptdJPanel(JPanel panelRight, Object valueAt, ptJPanel before) {
        initComponents();
        this.panelRight = panelRight;
        this.before = before;
        this.p = (TravelP) valueAt;
        this.loadData();
        this.setInput(false);
    }

    private void loadData() {
        this.hotelCurrent.setText("Hotel: " + this.p.getHotel());
        this.airlinerCurrent.setText("Airliner: " + this.p.getAirliner());
        this.guiderCurrent.setText("Guider: " + this.p.getGuider());
        this.profitCurrent.setText("Price: " + this.p.getPrice());
        for (HotelSupplier hs : TESystem.getDatabase().getUsers().getHotelSupplierDirectory()) {
            this.hotel.addItem(hs.getCompany().getCompanyName());
        }
        for (AirlinerSupplier as : TESystem.getDatabase().getUsers().getAirlinerSupplierDirectory()) {
            this.airliner.addItem(as.getCompany().getCompanyName());
        }
        for (Guider g : TESystem.getDatabase().getUsers().getGuiderDirectory()) {
            this.guider.addItem(g.getName());
        }
    }

    private void setInput(boolean enable) {
        this.hotel.setEnabled(enable);
        this.airliner.setEnabled(enable);
        this.guider.setEnabled(enable);
        this.profit.setEnabled(enable);
        this.updateBtn.setEnabled(!enable);
        this.saveBtn.setEnabled(enable);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        updateBtn = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        hotelCurrent = new javax.swing.JLabel();
        airlinerCurrent = new javax.swing.JLabel();
        guiderCurrent = new javax.swing.JLabel();
        profitCurrent = new javax.swing.JLabel();
        hotel = new javax.swing.JComboBox<>();
        airliner = new javax.swing.JComboBox<>();
        guider = new javax.swing.JComboBox<>();
        saveBtn = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        profit = new javax.swing.JTextField();

        setBackground(new java.awt.Color(204, 217, 216));

        updateBtn.setBackground(new java.awt.Color(235, 181, 146));
        updateBtn.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        updateBtn.setText("Uptate");
        updateBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateBtnActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Lucida Grande", 0, 24)); // NOI18N
        jLabel2.setText("Travel Product");

        hotelCurrent.setText("Hotel :");

        airlinerCurrent.setText("Airliner :");

        guiderCurrent.setText("Guider : ");

        profitCurrent.setText("Profit :");

        saveBtn.setBackground(new java.awt.Color(235, 181, 146));
        saveBtn.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        saveBtn.setText("Save");
        saveBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveBtnActionPerformed(evt);
            }
        });

        jButton7.setText("< Back");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(68, 68, 68)
                .addComponent(jButton7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 132, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(77, 77, 77)
                        .addComponent(jLabel2)
                        .addGap(369, 369, 369))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(hotelCurrent)
                                .addGap(89, 89, 89))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(airlinerCurrent)
                                    .addComponent(guiderCurrent)
                                    .addComponent(profitCurrent))
                                .addGap(77, 77, 77)))
                        .addGap(11, 11, 11)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(airliner, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(hotel, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(2, 2, 2)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(profit, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(guider, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(315, 315, 315))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(updateBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(saveBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(271, 271, 271))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(86, 86, 86)
                .addComponent(jLabel2)
                .addGap(61, 61, 61)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(hotelCurrent)
                            .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(33, 33, 33)
                        .addComponent(airlinerCurrent)
                        .addGap(49, 49, 49)
                        .addComponent(guiderCurrent)
                        .addGap(54, 54, 54)
                        .addComponent(profitCurrent))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(hotel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(29, 29, 29)
                        .addComponent(airliner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(38, 38, 38)
                        .addComponent(guider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(44, 44, 44)
                        .addComponent(profit, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 75, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(updateBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(saveBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(60, 60, 60))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void updateBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateBtnActionPerformed
        // TODO add your handling code here:
        this.setInput(true);

//        this;
    }//GEN-LAST:event_updateBtnActionPerformed

    private void saveBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveBtnActionPerformed
        // TODO add your handling code here:
        String profit = this.profit.getText();
        if (profit.equals("")) {
            this.setInput(false);
            for (AirlinerSupplier as : TESystem.getDatabase().getUsers().getAirlinerSupplierDirectory()) {
                if (as.getCompany().getCompanyName().equals(this.airliner.getSelectedItem())) {
                    this.p.setAirliner(as);
                }
            }
            for (HotelSupplier hs : TESystem.getDatabase().getUsers().getHotelSupplierDirectory()) {
                if (hs.getCompany().getCompanyName().equals(this.hotel.getSelectedItem())) {
                    this.p.setHotel(hs);
                }
            }
            for (Guider g : TESystem.getDatabase().getUsers().getGuiderDirectory()) {
                if (g.getName().equals(this.guider.getSelectedItem())) {
                    this.p.setGuider(g);
                }
            }

            JOptionPane.showMessageDialog(null, "Update succeed!!");
            this.jButton7ActionPerformed(evt);
            before.populateInitTable();

        } else {
            this.setInput(false);
            for (AirlinerSupplier as : TESystem.getDatabase().getUsers().getAirlinerSupplierDirectory()) {
                if (as.getCompany().getCompanyName().equals(this.airliner.getSelectedItem())) {
                    this.p.setAirliner(as);
                }
            }
            for (HotelSupplier hs : TESystem.getDatabase().getUsers().getHotelSupplierDirectory()) {
                if (hs.getCompany().getCompanyName().equals(this.hotel.getSelectedItem())) {
                    this.p.setHotel(hs);
                }
            }
            for (Guider g : TESystem.getDatabase().getUsers().getGuiderDirectory()) {
                if (g.getName().equals(this.guider.getSelectedItem())) {
                    this.p.setGuider(g);
                }
            }

            this.p.setPrice(Float.parseFloat(profit));
            JOptionPane.showMessageDialog(null, "Update succeed!!");
            this.jButton7ActionPerformed(evt);
            before.populateInitTable();
        }

    }//GEN-LAST:event_saveBtnActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        // TODO add your handling code here:
        CardLayout layout = (CardLayout) this.panelRight.getLayout();
        this.panelRight.remove(this);
        layout.previous(this.panelRight);
    }//GEN-LAST:event_jButton7ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> airliner;
    private javax.swing.JLabel airlinerCurrent;
    private javax.swing.JComboBox<String> guider;
    private javax.swing.JLabel guiderCurrent;
    private javax.swing.JComboBox<String> hotel;
    private javax.swing.JLabel hotelCurrent;
    private javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JTextField profit;
    private javax.swing.JLabel profitCurrent;
    private javax.swing.JButton saveBtn;
    private javax.swing.JButton updateBtn;
    // End of variables declaration//GEN-END:variables
}
